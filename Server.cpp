#include "Server.h"
#include "MyForm.h"
#include <cmath>


Server::Server(System::Windows::Forms::Form^ Parent) {
	ParentForm = Parent;
	InitializeWSASockets();
	InitUDPSocket();
	InitTCPSocket();
	InitClientList();
}


Server::~Server() {
	Shutdown();
}

// Initializing of arrays for storing the connections
void Server::InitClientList() {
	SocketList = gcnew array<SOCKET>(SB_MAX_CLIENTS);
	ClientList = gcnew array<ClientID*>(SB_MAX_CLIENTS);
	for (int i = 0; i < SB_MAX_CLIENTS; i++) {
		ClientID* tmp = new ClientID();
		ZeroMemory(tmp, sizeof(ClientID));
		memcpy(tmp->UserName, "/", sizeof(char));
		ClientList[i] = tmp;
		ClientList[i]->Ping = -1;
		SocketList[i] = SOCKET_ERROR;
	}
}

// Initializing WSA
int Server::InitializeWSASockets() {
	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD(2, 2);
	int err;
	err = WSAStartup(wVersionRequested, &wsaData);

	return err;
}

// Initializing of the UDP Socket and setting it up for Broadcasting
void Server::InitUDPSocket() {
	// Create UDP Socket
	UDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (UDPSocket == INVALID_SOCKET) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Socket function failed with error: " + WSAGetLastError());
	}
	else {
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Socket function succeeded");
	}

	BOOL bOptVal = TRUE;
	int bOptLen = sizeof(BOOL);

	int err = setsockopt(UDPSocket, SOL_SOCKET, SO_BROADCAST, (char *)&bOptVal, bOptLen);
	if (err == SOCKET_ERROR)
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP setsockopt for SO_BROADCAST failed with error: " + WSAGetLastError());
	else ((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Set SO_BROADCAST: ON");

	err = setsockopt(UDPSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&bOptVal, bOptLen);
	if (err == SOCKET_ERROR)
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP setsockopt for SO_REUSEADDR failed with error: " + WSAGetLastError());
	else ((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Set SO_REUSEADDR: ON");

	u_long Mode = 1;
	err = ioctlsocket(UDPSocket, FIONBIO, &Mode);
	if (err != 0) ((Chatter::MyForm^)ParentForm)->AddOutputText("UDP ioctlsocket failed with error: " + WSAGetLastError());
}

// Initializing of the TCP Socket and start listening for new connections
void Server::InitTCPSocket() {
	// Create TCP Socket
	TCPSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (TCPSocket == INVALID_SOCKET) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Socket function failed with error: " + WSAGetLastError());
	}
	else {
		((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Socket function succeeded");
	}

	// Bind Socket
	sockaddr_in serviceTCP = { 0 };
	serviceTCP.sin_family = AF_INET;
	serviceTCP.sin_addr.s_addr = *(u_long *)&GetHost();
	serviceTCP.sin_port = htons(SB_TCP_Port);

	int err = bind(TCPSocket, (SOCKADDR *)&serviceTCP, sizeof(serviceTCP));
	if (err == SOCKET_ERROR) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Bind failed with error: " + WSAGetLastError());
		closesocket(TCPSocket);
		WSACleanup();
		return;
	}
	else ((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Bind succeeded");

	u_long Mode = 1;
	err = ioctlsocket(TCPSocket, FIONBIO, &Mode);
	if (err != 0)
		((Chatter::MyForm^)ParentForm)->AddOutputText("TCP ioctlsocket failed with error: " + WSAGetLastError());

	// Listen Socket
	err = listen(TCPSocket, 0);
	if (err == SOCKET_ERROR) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Listen failed with error: " + WSAGetLastError());
		closesocket(TCPSocket);
		WSACleanup();
		return;
	}
	else ((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Listen succeeded");
}

// Starts our thread for broadcasting only
void Server::Advertise() {
	AdvertiseThread = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(this, &Server::Advertise_MT));
	AdvertiseThread->Start();
}

// Advertising via UDP to inform Clients the server is waiting for connections
void Server::Advertise_MT() {
	while (true && !shutdownThreads) {
		// Send
		UDPPackage package;
		sockaddr_in service = { 0 };
		service.sin_family = AF_INET;
		service.sin_addr.s_addr = INADDR_BROADCAST;
		service.sin_port = htons(SB_UDP_Port);
		int err = sendto(UDPSocket, reinterpret_cast<char*>(&package), sizeof(UDPPackage), 0, reinterpret_cast<sockaddr*>(&service), sizeof(service));
		if (err == SOCKET_ERROR) {
			if (WSAGetLastError() != WSAEWOULDBLOCK) {
				((Chatter::MyForm^)ParentForm)->AddOutputText("Send failed with error: " + WSAGetLastError());
				WSACleanup();
				return;
			}
		}
		else {
			//	((Chatter::MyForm^)ParentForm)->AddOutputText("Bytes send: " + err);
		}
		Sleep(1000);
	}
}

// Adds a client to our array of connections after connected via TCP
void Server::AddClient(SignInPackage signInPackage, SOCKET socket) {
	unsigned int Pos = 0;
	bool IsNewClient = true;
	for each (ClientID* client in ClientList) {
		if (client->UserName[0] != '/'){
			if (*client == signInPackage.Client) {
				IsNewClient = false;
				break;
			}
			Pos++;
		}
		else {
			break; 
		}
	}
	if (IsNewClient) {
		*ClientList[Pos] = signInPackage.Client;
		SocketList[Pos] = socket;
		System::String^ Name = gcnew System::String(signInPackage.Client.UserName);
		System::String^ IP = gcnew System::String(inet_ntoa(signInPackage.Client.IPAddress));
		((Chatter::MyForm^)ParentForm)->AddOutputText("New Client " + Name + " with IP " + IP + " added.");
	}
}

// Starts the thread for waiting and connecting with a client
void Server::AcceptClient() {
	NetworkThread = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(this, &Server::AcceptClient_MT));
	NetworkThread->Start();
}

// Accepts the a client connection and waits for information (ping, message,...)
void Server::AcceptClient_MT() {
	bool NewConnection = false;
	SOCKET OpenConnectionWaitingForAdd;


	LONGLONG g_Frequency, g_CurentCount, g_LastCount, g_LastSendCount;
	double dTimeDiff = 0;
	double SendTime = 2;
	double Timer = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&g_Frequency);
	QueryPerformanceCounter((LARGE_INTEGER*)&g_LastSendCount);
	while (true && !shutdownThreads) {
		SOCKET AcceptSocket;
		// Accept Socket
		AcceptSocket = accept(TCPSocket, NULL, NULL);
		if (AcceptSocket == INVALID_SOCKET) {
			if (WSAGetLastError() != WSAEWOULDBLOCK)
				((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Accept failed with error:" + WSAGetLastError());
		}
		else {
			NewConnection = true;
			OpenConnectionWaitingForAdd = AcceptSocket;
		}

		if (NewConnection) {
			SignInPackage signInPackage;
			int err = recv(OpenConnectionWaitingForAdd, reinterpret_cast<char*>(&signInPackage), sizeof(signInPackage), 0);
			if (err > 0) {
				AddClient(signInPackage, OpenConnectionWaitingForAdd);
				NewConnection = false;
				OpenConnectionWaitingForAdd = NULL;
				SendClientList();
			}
		}

		// Receive & Send
		for (int i = 0; i < SB_MAX_CLIENTS; i++) {
			if (SocketList[i] == SOCKET_ERROR) continue;

			DWORD Header;
			int err = recv(SocketList[i], reinterpret_cast<char*>(&Header), sizeof(DWORD), 0);
			if (err > 0) {

				// Recv Pong
				if (Header == 'PONG') {
					CPongPackage pongRecvPackage;
					for (int j = 0; j < SB_MAX_CLIENTS; j++) {
						if (SocketList[j] == SOCKET_ERROR) {
							continue;
						}
						int err = recv(SocketList[j], reinterpret_cast<char*>(&pongRecvPackage), sizeof(CPongPackage), 0);
						if (err > 0) {
							if (pongRecvPackage.Mode == 1) {
								PongPackage sendPackage;
								sendPackage.Header = 'PONG';
								sendPackage.Mode = pongRecvPackage.Mode;
								sendPackage.TimeStamp = pongRecvPackage.TimeStamp;
								int err = send(SocketList[j], reinterpret_cast<char*>(&sendPackage), sizeof(PongPackage), 0);
							}
							else {
								QueryPerformanceCounter((LARGE_INTEGER*)&g_LastCount);
								dTimeDiff = (((double)(g_LastCount - pongRecvPackage.TimeStamp)) / ((double)g_Frequency));
								ClientList[i]->Ping = (int) std::round(dTimeDiff * 1000);
								//((Chatter::MyForm^)ParentForm)->AddOutputText("Pong returned after " + dTimeDiff * 1000 + "ms.");
							}
						}
						else if (err == 0) {
							((Chatter::MyForm^)ParentForm)->AddOutputText("Connection closed");
							ZeroMemory(ClientList[j], sizeof(ClientID));
							memcpy(ClientList[j]->UserName, "/", sizeof(char));
							SocketList[j] = SOCKET_ERROR;
						}
						else if (WSAGetLastError() == WSAEWOULDBLOCK) {

						}
						else {
							if (WSAGetLastError() == WSAECONNRESET) {
								ZeroMemory(ClientList[j], sizeof(ClientID));
								memcpy(ClientList[j]->UserName, "/", sizeof(char));
								SocketList[j] = SOCKET_ERROR;
							}
							else {
								((Chatter::MyForm^)ParentForm)->AddOutputText("recv failed with error: " + WSAGetLastError());
							}
						}
					}
				}

				// Send Msg
				else if (Header == 'CMSG') {
					CMessagePackage recvPackage;
					err = recv(SocketList[i], reinterpret_cast<char*>(&recvPackage), sizeof(CMessagePackage), 0);
					if (err > 0) {
						System::String^ username = gcnew System::String(recvPackage.UserName);
						System::String^ msg = gcnew System::String(recvPackage.Msg);
						((Chatter::MyForm^)ParentForm)->AddOutputText(username + ": " + msg);
						for (int j = 0; j < SB_MAX_CLIENTS; j++) {
							if (SocketList[j] == SOCKET_ERROR || i == j) {
								continue;
							}
							MessagePackage sendPackage;
							sendPackage.Header = 'CMSG';
							strcpy_s(sendPackage.Msg, recvPackage.Msg);
							strcpy_s(sendPackage.UserName, recvPackage.UserName);
							sendPackage.Port = recvPackage.Port;
							sendPackage.Size = recvPackage.Size;
							err = send(SocketList[j], reinterpret_cast<char*>(&sendPackage), sizeof(MessagePackage), 0);
						}
						delete username;
						delete msg;
					}
					else if (err == 0) {
						((Chatter::MyForm^)ParentForm)->AddOutputText("Connection closed");
						ZeroMemory(ClientList[i], sizeof(ClientID));
						memcpy(ClientList[i]->UserName, "/", sizeof(char));
						SocketList[i] = SOCKET_ERROR;
					}
					else if (WSAGetLastError() == WSAEWOULDBLOCK) {

					}
					else {
						if (WSAGetLastError() == WSAECONNRESET) {
							ZeroMemory(ClientList[i], sizeof(ClientID));
							memcpy(ClientList[i]->UserName, "/", sizeof(char));
							SocketList[i] = SOCKET_ERROR;
						}
						else {
							((Chatter::MyForm^)ParentForm)->AddOutputText("recv failed with error: " + WSAGetLastError());
						}
					}
				}
			}
		}

		// Send Ping
		QueryPerformanceCounter((LARGE_INTEGER*)&g_LastCount);
		Timer = (((double)(g_LastCount - g_LastSendCount)) / ((double)g_Frequency));
		if (Timer > SendTime) {
			QueryPerformanceCounter((LARGE_INTEGER*)&g_LastSendCount);
			QueryPerformanceCounter((LARGE_INTEGER*)&g_CurentCount);
			// Send Ping
			PongPackage pongSendPackage;
			pongSendPackage.Mode = 0;
			pongSendPackage.TimeStamp = g_CurentCount;
			((Chatter::MyForm^)ParentForm)->ClearPingField();
			SendClientList();
			for (int j = 0; j < SB_MAX_CLIENTS; j++) {
				if (SocketList[j] == SOCKET_ERROR) {
					continue;
				}
				int err = send(SocketList[j], reinterpret_cast<char*>(&pongSendPackage), sizeof(PongPackage), 0);
				((Chatter::MyForm^)ParentForm)->AddPingFieldText("" + (int)round(ClientList[j]->Ping));
			}
			Timer = 0;
		}
	}
}

// Sends all clients the list of all clients for displaying the connected clients visually for everyone
void Server::SendClientList() {
	NotificationPackage NotPackage;
	((Chatter::MyForm^)ParentForm)->ClearClientField();
	for (int i = 0; i < SB_MAX_CLIENTS; i++) {
		NotPackage.Clients[i] = *ClientList[i];
		if (NotPackage.Clients[i].UserName[0] != '/') {
			System::String^ username = gcnew System::String(NotPackage.Clients[i].UserName);
			((Chatter::MyForm^)ParentForm)->AddClient(username);
		}
	}

	for (int j = 0; j < SB_MAX_CLIENTS; j++) {
		if (SocketList[j] == SOCKET_ERROR) {
			continue;
		}
		int err = send(SocketList[j], reinterpret_cast<char*>(&NotPackage), sizeof(NotificationPackage), 0);
	}


}

// Cleaning up threads and arrays
void Server::Shutdown() {
	shutdownThreads = true;
	NetworkThread->Join();
	delete NetworkThread;
	for each (ClientID* client in ClientList) {
		delete client;
	}
	delete ClientList;
	delete SocketList;
}

// Find local ip
IN_ADDR Server::GetHost() {
	char ac[80];
	if (gethostname(ac, sizeof(ac)) == SOCKET_ERROR) {
		//tmp.append() << "Error " << WSAGetLastError() << " when getting local host name." << std::endl;
	}
	System::String^ tmp = gcnew System::String(ac);
	((Chatter::MyForm^)ParentForm)->AddOutputText("Host name is " + tmp + ".");
	delete tmp;

	struct hostent *phe = gethostbyname(ac);
	if (phe == 0) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("Bad host lookup.");
	}

	for (int i = 0; phe->h_addr_list[i] != 0; ++i) {
		struct in_addr addr;
		memcpy(&addr, phe->h_addr_list[i], sizeof(struct in_addr));
		System::String^ tmp = gcnew System::String(inet_ntoa(addr));
		((Chatter::MyForm^)ParentForm)->AddOutputText("Address " + i + ": " + tmp);
		delete tmp;
		return addr;
	}
	IN_ADDR tmpAddr;
	return tmpAddr;
}
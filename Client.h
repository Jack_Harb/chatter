#pragma once
#include "winsock2.h"
#include "Packages.h"

public ref class Client
{
public:
	Client(System::Windows::Forms::Form^ Parent, const char* INUserName);
	~Client();

private:
	System::Windows::Forms::Form^ ParentForm;
	SOCKET UDPSocket, TCPSocket;
	array<ClientID*>^ ClientList;
	System::Threading::Thread^  NetworkThread;
	sockaddr_in* ServerAddress;
	bool shutdownThreads = false;
	const char* msg = nullptr;
	const char* UserName = "";
	unsigned short ServerTCPPort = 0;

	int InitializeWSASockets(); 
	void InitClientList();
	void ConnectToServer_MT();
	void CheckPromotion();

	IN_ADDR GetHost();

public:
	void InitUDPSocket();
	UDPPackage Listen();
	void ShutDown();
	void SendMessage(const char* Message);
	void ConnectToServer(UDPPackage Package);
};


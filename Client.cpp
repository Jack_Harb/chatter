#include "Client.h"
#include "MyForm.h"
#include <iostream>

Client::Client(System::Windows::Forms::Form^ Parent, const char* INUserName) {
	ParentForm = Parent;
	UserName = INUserName;
	InitializeWSASockets();
	InitUDPSocket();
	InitClientList();
	ServerAddress = new sockaddr_in;
}


Client::~Client() {
	ShutDown();
}

// Initilizing of client list as array
void Client::InitClientList() {
	ClientList = gcnew array<ClientID*>(SB_MAX_CLIENTS);
	for (int i = 0; i < SB_MAX_CLIENTS; i++) {
		ClientID* tmp = new ClientID();
		ZeroMemory(tmp, sizeof(ClientID));
		memcpy(tmp->UserName, "/", sizeof(char));
		ClientList[i] = tmp;
	}

}

// Initializing WSA
int Client::InitializeWSASockets() {
	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD(2, 2);
	int err = WSAStartup(wVersionRequested, &wsaData);
	return err;
}

// Initializing of the UDP Socket and bind for every address
void Client::InitUDPSocket() {
	// Create UDP Socket
	UDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (UDPSocket == INVALID_SOCKET) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Socket function failed with error: " + WSAGetLastError());
	}
	else {
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Socket function succeeded");
	}

	u_long bOptVal = TRUE;
	int bOptLen = sizeof(u_long);

	int err = setsockopt(UDPSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&bOptVal, bOptLen);
	if (err == SOCKET_ERROR) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP setsockopt for SO_REUSEADDR failed with error: " + WSAGetLastError());
	}
	else ((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Set SO_REUSEADDR: ON");

	u_long Mode = 1;
	err = ioctlsocket(UDPSocket, FIONBIO, &Mode);
	if (err != 0) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP ioctlsocket failed with error: " + WSAGetLastError());
	}

	sockaddr_in service = { 0 };
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = INADDR_ANY;
	service.sin_port = htons(SB_UDP_Port);

	err = bind(UDPSocket, (SOCKADDR *)&service, sizeof(service));
	if (err == SOCKET_ERROR) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Bind failed with error: " + WSAGetLastError());
		closesocket(UDPSocket);
		WSACleanup();
		return;
	}
	else {
		((Chatter::MyForm^)ParentForm)->AddOutputText("UDP Bind succeeded");
	}		
}

// Initializing of TCP Socket and trying to connect to the given address received via UDP Broadcasting from Server
// If connected sending and receiving information (ping, message, ...)
void Client::ConnectToServer_MT(){

	// Create Socket
	TCPSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (TCPSocket == INVALID_SOCKET) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Socket function failed with error: " + WSAGetLastError());
	}
	else {
		((Chatter::MyForm^)ParentForm)->AddOutputText("TCP Socket function succeeded");
	}
	
	((Chatter::MyForm^)ParentForm)->AddOutputText(" ");
	((Chatter::MyForm^)ParentForm)->AddOutputText("-------------------------");
	((Chatter::MyForm^)ParentForm)->AddOutputText("--- Trying to connect ---");
	((Chatter::MyForm^)ParentForm)->AddOutputText("-------------------------");
	((Chatter::MyForm^)ParentForm)->AddOutputText(" ");
	
	// Connect Socket
	sockaddr_in service = { 0 };
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = *(u_long *)&ServerAddress->sin_addr;
	service.sin_port = htons(ServerTCPPort);

	bool connectionLost = false;

	int err = connect(TCPSocket, (SOCKADDR *)&service, sizeof(service));
	if (err == SOCKET_ERROR) {
		if (WSAGetLastError() != WSAEWOULDBLOCK) {
			((Chatter::MyForm^)ParentForm)->AddOutputText("Connected failed with error: " + WSAGetLastError());
			closesocket(TCPSocket);
			WSACleanup();
			return;
		}
	}
	else {

		((Chatter::MyForm^)ParentForm)->AddOutputText("Connected to Server");

		// Send ClientInformations
		SignInPackage signInPackage;
		strcpy_s(signInPackage.Client.UserName, UserName);
		signInPackage.Client.IPAddress = GetHost();
		err = send(TCPSocket, reinterpret_cast<char*>(&signInPackage), signInPackage.Size, 0);

		u_long Mode = 1;
		err = ioctlsocket(TCPSocket, FIONBIO, &Mode);
		if (err != 0) {
			((Chatter::MyForm^)ParentForm)->AddOutputText("TCP ioctlsocket failed with error: " + WSAGetLastError());
		}

		
		LONGLONG g_Frequency, g_CurentCount, g_LastCount, g_LastSendCount, g_LastReceiveTime;
		double dTimeDiff = 0;
		double SendTime = 3;
		double Timer = 0;
		double TimeOutTime = 10;
		QueryPerformanceFrequency((LARGE_INTEGER*)&g_Frequency);
		QueryPerformanceCounter((LARGE_INTEGER*)&g_LastSendCount);
		QueryPerformanceCounter((LARGE_INTEGER*)&g_LastReceiveTime);
		while (true && !shutdownThreads) {
			DWORD Header;
			err = recv(TCPSocket, reinterpret_cast<char*>(&Header), sizeof(DWORD), 0);
			if (err > 0) {
				if (Header == 'NOTI') {
					// Receive Notification of Client Add/Remove
					CNotificationPackage recvNotPackage;
					err = recv(TCPSocket, reinterpret_cast<char*>(&recvNotPackage), sizeof(CNotificationPackage), 0);
					if (err > 0) {
						((Chatter::MyForm^)ParentForm)->ClearClientField();
						((Chatter::MyForm^)ParentForm)->ClearPingField();
						for (int i = 0; i < SB_MAX_CLIENTS; i++) {
							if (recvNotPackage.Clients[i].UserName[0] != '/') {
								ClientList[i]->Ping = recvNotPackage.Clients[i].Ping;
								ClientList[i]->IPAddress = recvNotPackage.Clients[i].IPAddress;
								strcpy_s(ClientList[i]->UserName, recvNotPackage.Clients[i].UserName);

								System::String^ username = gcnew System::String(recvNotPackage.Clients[i].UserName);
								((Chatter::MyForm^)ParentForm)->AddClient(username);
								((Chatter::MyForm^)ParentForm)->AddPingFieldText(""+recvNotPackage.Clients[i].Ping);
							}
						}
					}
					else if (err == 0) {
						((Chatter::MyForm^)ParentForm)->AddOutputText("Connection closed: " + WSAGetLastError());
						((Chatter::MyForm^)ParentForm)->ClearClientField();
						break;
					}
					else if (WSAGetLastError() == WSAEWOULDBLOCK) {
					}
					else if (WSAGetLastError() == WSAECONNRESET) {
						((Chatter::MyForm^)ParentForm)->AddOutputText("Connection abort");
						((Chatter::MyForm^)ParentForm)->ClearClientField();
						break;
					}
					else {
						((Chatter::MyForm^)ParentForm)->AddOutputText("recv failed with error: " + WSAGetLastError());
					}
				}

				else if (Header == 'CMSG') {
					// Receive Msg
					CMessagePackage recvMsgPackage;
					err = recv(TCPSocket, reinterpret_cast<char*>(&recvMsgPackage), sizeof(CMessagePackage), 0);
					if (err > 0) {
						System::String^ username = gcnew System::String(recvMsgPackage.UserName);
						System::String^ msg = gcnew System::String(recvMsgPackage.Msg);
						((Chatter::MyForm^)ParentForm)->AddOutputText(username + ": " + msg);
					}
					else if (err == 0) {
						((Chatter::MyForm^)ParentForm)->AddOutputText("Connection closed: " + WSAGetLastError());
						((Chatter::MyForm^)ParentForm)->ClearClientField();
						break;
					}
					else if (WSAGetLastError() == WSAEWOULDBLOCK) {
					}
					else if (WSAGetLastError() == WSAECONNRESET) {
						((Chatter::MyForm^)ParentForm)->AddOutputText("Connection abort");
						((Chatter::MyForm^)ParentForm)->ClearClientField();
						break;
					}
					else {
						((Chatter::MyForm^)ParentForm)->AddOutputText("recv failed with error: " + WSAGetLastError());
					}
				}

				else if (Header == 'PONG') {
					// Receive Pong
					CPongPackage recvPongPackage;
					err = recv(TCPSocket, reinterpret_cast<char*>(&recvPongPackage), sizeof(CPongPackage), 0);
					if (err > 0) {
						if (recvPongPackage.Mode == 0) {
							PongPackage sendPackage;
							sendPackage.Header = 'PONG';
							sendPackage.Mode = recvPongPackage.Mode;
							sendPackage.TimeStamp = recvPongPackage.TimeStamp;

							err = send(TCPSocket, reinterpret_cast<char*>(&sendPackage), sizeof(PongPackage), 0);
						}
						else {
							QueryPerformanceCounter((LARGE_INTEGER*)&g_LastReceiveTime);				
							//QueryPerformanceCounter((LARGE_INTEGER*)&g_LastCount);
							//dTimeDiff = (((double)(g_LastCount - recvPongPackage.TimeStamp)) / ((double)g_Frequency));
							//((Chatter::MyForm^)ParentForm)->AddOutputText("Pong returned after " + dTimeDiff * 1000 + "ms.");
						}
					}
					else if (err == 0) {
						((Chatter::MyForm^)ParentForm)->AddOutputText("Connection closed: " + WSAGetLastError());
						((Chatter::MyForm^)ParentForm)->ClearClientField();
						break;
					}
					else if (WSAGetLastError() == WSAEWOULDBLOCK) {
					}
					else if (WSAGetLastError() == WSAECONNRESET) {
						((Chatter::MyForm^)ParentForm)->AddOutputText("Connection abort");
						((Chatter::MyForm^)ParentForm)->ClearClientField();
						break;
					}
					else {
						((Chatter::MyForm^)ParentForm)->AddOutputText("recv failed with error: " + WSAGetLastError());
					}
				}
			}
			// Send Ping
			QueryPerformanceCounter((LARGE_INTEGER*)&g_LastCount);
			Timer = (((double)(g_LastCount - g_LastSendCount)) / ((double)g_Frequency));
			TimeOutTime = (((double)(g_LastCount - g_LastReceiveTime)) / ((double)g_Frequency));
			if (TimeOutTime > 10) {
				connectionLost = true;
				((Chatter::MyForm^)ParentForm)->AddOutputText("Connection Lost!");
				break;
			}
			else if (Timer > SendTime) {
				QueryPerformanceCounter((LARGE_INTEGER*)&g_CurentCount);
				QueryPerformanceCounter((LARGE_INTEGER*)&g_LastSendCount);
				// Send Ping
				PongPackage sendPackage;
				sendPackage.Header = 'PONG';
				sendPackage.Mode = 1;
				sendPackage.TimeStamp = g_CurentCount;

				err = send(TCPSocket, reinterpret_cast<char*>(&sendPackage), sizeof(PongPackage), 0);
				Timer = 0;
			}

			// Send Message
			if (msg != nullptr) {
				MessagePackage sendPackage;
				strcpy_s(sendPackage.UserName, UserName);
				strcpy_s(sendPackage.Msg, msg);
				err = send(TCPSocket, reinterpret_cast<char*>(&sendPackage), sendPackage.Size, 0);
				if (err > 0) {
					msg = nullptr;
				}
				else if (err == 0) {
					((Chatter::MyForm^)ParentForm)->AddOutputText("Connection closed: " + WSAGetLastError());
					((Chatter::MyForm^)ParentForm)->ClearClientField();
					break;
				}
				else if (WSAGetLastError() == WSAEWOULDBLOCK) {
					continue;
				}
				else
					((Chatter::MyForm^)ParentForm)->AddOutputText("Send failed with error: " + WSAGetLastError());

			}
		}
	}
	shutdown(TCPSocket, SD_BOTH);
	if (connectionLost) {
		CheckPromotion();
	}
}

// Check after losing connection to server, if we are the next possible Server to automatically host
void Client::CheckPromotion() {
	bool IsThisNextServer = true;
	IN_ADDR ThisAddress = GetHost();
	IN_ADDR NewAddress = ThisAddress;
	for (int i = 0; i < SB_MAX_CLIENTS; i++){
		if (ClientList[i]->UserName[0] != '/' && ClientList[i]->IPAddress.S_un.S_addr < ThisAddress.S_un.S_addr) {
			IsThisNextServer = false;
			if (ClientList[i]->IPAddress.S_un.S_addr < NewAddress.S_un.S_addr) {
				NewAddress = ClientList[i]->IPAddress;
			}
		}
	}
	IsThisNextServer = false;
	if (IsThisNextServer) {
		((Chatter::MyForm^)ParentForm)->PromoteToServer();
	}
	else {
		((Chatter::MyForm^)ParentForm)->ResetClient();
	}
}

// Listen on UDP Socket for incoming broadcasts from server to receive TCP information
UDPPackage Client::Listen() {
	// Receive
	UDPPackage recvPackage;
	UDPPackage normalPackage;
	UDPPackage myPackage;
	myPackage.Header = 'Def';

	bool isMyPackage = false;
	int ServerAddressSize = sizeof(*ServerAddress);

	int err = recvfrom(UDPSocket, reinterpret_cast<char*>(&recvPackage), sizeof(UDPPackage), 0, reinterpret_cast<sockaddr*>(ServerAddress), &ServerAddressSize);
	if (err > 0) {
		isMyPackage = err == sizeof(UDPPackage) && recvPackage.Header == normalPackage.Header;
		if (isMyPackage) {
			((Chatter::MyForm^)ParentForm)->AddOutputText("UDP: Bytes received: " + err);
			return recvPackage;
		}
	}
	else if (err == 0) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("Connection closed");
		wprintf(L"Connection closed\n");
	}
	else if (WSAGetLastError() == WSAEWOULDBLOCK) {

	}
	else {
		((Chatter::MyForm^)ParentForm)->AddOutputText("recv failed with error: " + WSAGetLastError());
	}
	Sleep(100);
	return myPackage;
}

// Starting the Networkthread with given information
void Client::ConnectToServer(UDPPackage Package) {
	ServerTCPPort = Package.Port;
	NetworkThread = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(this, &Client::ConnectToServer_MT));
	NetworkThread->Start();
}

// Simple UI Call, sets Message
void Client::SendMessage(const char* Message) {
	msg = Message;
}

// Cleanup WSA, Threads and Arrays
void Client::ShutDown() {
	closesocket(UDPSocket);
	WSACleanup();
	shutdownThreads = true;
	NetworkThread->Abort();
	delete NetworkThread;
	delete[] ClientList;
}

// Find local ip
IN_ADDR Client::GetHost() {
	char ac[80];
	if (gethostname(ac, sizeof(ac)) == SOCKET_ERROR) {
		//tmp.append() << "Error " << WSAGetLastError() << " when getting local host name." << std::endl;
	}
	System::String^ tmp = gcnew System::String(ac);
	((Chatter::MyForm^)ParentForm)->AddOutputText("Host name is " + tmp + ".");
	delete tmp;

	struct hostent *phe = gethostbyname(ac);
	if (phe == 0) {
		((Chatter::MyForm^)ParentForm)->AddOutputText("Bad host lookup.");
	}

	for (int i = 0; phe->h_addr_list[i] != 0; ++i) {
		struct in_addr addr;
		memcpy(&addr, phe->h_addr_list[i], sizeof(struct in_addr));
		System::String^ tmp = gcnew System::String(inet_ntoa(addr));
		((Chatter::MyForm^)ParentForm)->AddOutputText("Address " + i + ": " + tmp);
		delete tmp;
		return addr;
	}
	IN_ADDR addr;
	return addr;
}
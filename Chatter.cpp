// MultiThreadingServerClient.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "Server.h"
#include "Client.h"

void CheckForInput(Client* ChatClient) {
	std::cout << "Write your msg:" << std::endl;
	std::string strtmp;
	while (true) {
		std::cin >> strtmp;
		std::cout << std::endl;
		if (strtmp == "q" || strtmp == "Q") {
			break;
		}
		else {
			ChatClient->SendMessage(strtmp.c_str());
		}
	}
}

int main(){
	char Answer = '0';
	std::string tmp;
	std::cout << "You want to be the Server or the Client? [S] Server / [C] Client" << std::endl;
	std::cin >> Answer;
	std::cout << std::endl;

	Server* ChatServer = nullptr;
	Client* ChatClient = nullptr;

	// Server Mode
	if (Answer == 's' || Answer == 'S') {
		std::cout << "You are now the Server" << std::endl;
		ChatServer = new Server();
		ChatServer->AcceptClient();

		while (true) {
			ChatServer->Advertise();
		}
	}

	// Client Mode
	else if (Answer == 'c' || Answer == 'C') {
		std::cout << "You are now a Client" << std::endl;
		std::cout << "What ist your Username" << std::endl;
		std::cin >> tmp;
		std::cout << std::endl;

		const char* UserName = tmp.c_str();
		ChatClient = new Client(UserName);
		UDPPackage myPackage;
		myPackage.Header = 'Def';

		while (true) {
			myPackage = ChatClient->Listen();
			if (myPackage.Header != 'Def') {
				break;
			}
		}

		ChatClient->ConnectToServer(myPackage);
		
		Sleep(100);
		CheckForInput(ChatClient);
	}

	system("Pause");

	if (ChatClient) delete ChatClient;
	if (ChatServer) delete ChatServer;
    return 0;
}


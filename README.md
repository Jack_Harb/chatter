# README #

### What is this repository for? ###

* A little program for chatting with UDP Broadcasting and TCP Connection.
* Uses simple Multithreading for maintaining connection to several clients and keep broadcasting while accepting new Clients
* Its still in development and just for fun and testing out new things.

### How do I get set up? ###

* Run on the Visual Studio 2015 and higher (Visual C++ Redistributable 2014 and 2015)
* Build in Debug Mode and run the executable
* run in LAN or on local machine multiple instances of Chatter
* chose the server by typing in 'S' or 's' and confirm by pressing Enter
* chose for the rest of the instances 'C' or 'c' for client
* chat :)
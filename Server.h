#pragma once
#include "winsock2.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "Packages.h"

public ref class Server
{
public:
	Server(System::Windows::Forms::Form^ Parent);
	~Server();

private:
	System::Windows::Forms::Form^ ParentForm;
	array<ClientID*>^ ClientList;
	array<SOCKET>^ SocketList;
	SOCKET UDPSocket, TCPSocket;
	bool shutdownThreads = false;
	const char* msg = "";
	System::Threading::Thread^ NetworkThread;
	System::Threading::Thread^ AdvertiseThread;

	int InitializeWSASockets();
	void InitUDPSocket();
	void InitTCPSocket();
	void InitClientList();

	void AddClient(SignInPackage signInPackage, SOCKET socket);
	void AcceptClient_MT(); 
	void Advertise_MT();

	void SendClientList();
	IN_ADDR GetHost();

public:
	void Advertise();
	void AcceptClient();
	void Shutdown();
};


// MultiThreadingServerClient.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//
#include "MyForm.h"
#include <iostream>
#include <string>


using namespace System;
using namespace System::Windows::Forms;

int Main(array<String^>^ args) {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	Chatter::MyForm^ Form = gcnew Chatter::MyForm();
	Application::Run(Form);
	return 0;
}


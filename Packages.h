#pragma once
#include "winsock2.h"

#define SB_TCP_Port 8080
#define SB_UDP_Port 8079
#define SB_MAX_CLIENTS 10

struct ClientID {
	char UserName[50];
	int Ping;
	IN_ADDR IPAddress;

	bool operator==(ClientID Client) {
		// Normally Check for same IP instead of Name cause its unique, but for testing purpose Name is checked
		// return Client.IPAddress.S_un.S_addr.Equals(IPAddress.S_un.S_addr);
		return strcmp(UserName, Client.UserName) == 0;
	}

	ClientID() : Ping(-1)
	{
	}
};

// Server
#pragma pack(push,1)
struct MessagePackage
{
	DWORD Header;
	char UserName[50];
	unsigned short Port;
	char Msg[100];
	unsigned int Size;
	MessagePackage()
		:Header('CMSG')
		, Port(SB_TCP_Port)
		, Msg("")
		, UserName("Default")
		, Size(sizeof(DWORD) + sizeof(char[50]) + sizeof(unsigned short) + sizeof(char[100]) + sizeof(unsigned int))
	{
	}
};
#pragma pack(pop)

#pragma pack(push,1)
struct NotificationPackage
{
	DWORD Header;
	ClientID Clients[SB_MAX_CLIENTS];
	unsigned int Size;
	NotificationPackage()
		: Header('NOTI')
		, Size(sizeof(DWORD) + sizeof(unsigned int) + sizeof(ClientID[SB_MAX_CLIENTS]))
	{
		ZeroMemory(Clients, sizeof(ClientID[SB_MAX_CLIENTS]));
	}
};
#pragma pack(pop)

#pragma pack(push,1)
struct SignInPackage
{
	DWORD Header;
	ClientID Client;
	unsigned int Size;
	SignInPackage()
		: Header('SIGN')
		, Size(sizeof(DWORD) + sizeof(unsigned int) + sizeof(ClientID))
	{
	}
};
#pragma pack(pop)

#pragma pack(push,1)
struct UDPPackage
{
	DWORD Header;
	unsigned short Port;
	UDPPackage()
		:Header('GABP')
		, Port(SB_TCP_Port)
	{
	}
};
#pragma pack(pop)

#pragma pack(push,1)
struct PongPackage {
	DWORD Header;
	int Mode;
	LONGLONG TimeStamp;
	PongPackage()
		: Header('PONG')
		, Mode(0)
		, TimeStamp(0)
	{
	}
};
#pragma pack(pop)


// Client
#pragma pack(push,1)
struct CMessagePackage {
	char UserName[50];
	unsigned short Port;
	char Msg[100];
	unsigned int Size;
	CMessagePackage()
		: Port(SB_TCP_Port)
		, Msg("")
		, UserName("Default")
		, Size(sizeof(char[50]) + sizeof(unsigned short) + sizeof(char[100]) + sizeof(unsigned int))
	{
	}
};
#pragma pack(pop)

#pragma pack(push,1)
struct CNotificationPackage {
	ClientID Clients[SB_MAX_CLIENTS];
	unsigned int Size;
	CNotificationPackage()
		: Size(sizeof(unsigned int) + sizeof(ClientID[SB_MAX_CLIENTS]))
	{
		ZeroMemory(Clients, sizeof(ClientID[SB_MAX_CLIENTS]));
	}
};
#pragma pack(pop)

#pragma pack(push,1)
struct CPongPackage {
	int Mode;
	LONGLONG TimeStamp;
	CPongPackage()
		: Mode(0)
		, TimeStamp(0)
	{
	}
};
#pragma pack(pop)
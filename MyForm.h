#pragma once
#include "Server.h"
#include "Client.h"

namespace Chatter {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
		}

	protected:
		~MyForm()
		{
			CleanUp();
			if (components)
			{
				delete components;
			}
		}
	private: 
		System::Windows::Forms::TextBox^  Outputfield;
		System::Windows::Forms::TextBox^  InputField; 
		System::Windows::Forms::TextBox^  ClientsField;
		System::Windows::Forms::TextBox^  Pingfield;
		System::ComponentModel::Container ^components;

		Server^ ChatServer = nullptr;
		Client^ ChatClient = nullptr;
		System::String^ Answer = "";
		System::String^ UserName = "";
	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;

		bool ChatMode = false;

		delegate void AddOutputTextDelegate(System::String^ Text);
		delegate void ClearInputFieldDelegate();

		delegate void AddClientFieldTextDelegate(System::String^ Text);
		delegate void ClearClientFieldDelegate();

		delegate void AddPingFieldTextDelegate(System::String^ Text);
		delegate void ClearPingFieldDelegate();

		delegate void PromoteToServerDelegate();
		delegate void ResetClientDelegate();

	public:
		void PromoteToServer() {
			if (Outputfield->InvokeRequired) {
				PromoteToServerDelegate^ d = gcnew PromoteToServerDelegate(this, &MyForm::PromoteToServer);
				Invoke(d, gcnew array<Object^>{});
			}
			else {
				CleanUp();
				// Server Mode
				Outputfield->Clear();
				AddOutputText("You are now the Server!");
				ChatServer = gcnew Server(this);
				ChatServer->Advertise();
				ChatServer->AcceptClient();
			}
		}
		void ResetClient() {
			if (Outputfield->InvokeRequired) {
				PromoteToServerDelegate^ d = gcnew PromoteToServerDelegate(this, &MyForm::ResetClient);
				Invoke(d, gcnew array<Object^>{});
			}
			else {
				CleanUp();
				// Client Mode
				Outputfield->Clear();
				AddOutputText("You are now the Server!");
				ChatClient = gcnew Client(this, (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(UserName)).ToPointer());
				UDPPackage myPackage;
				while (true) {
					myPackage = ChatClient->Listen();
					if (myPackage.Header != 'Def') {
						break;
					}
				}
				ChatClient->ConnectToServer(myPackage);
			}
		}

		void AddOutputText(System::String^ Text) {
			if (Outputfield->InvokeRequired) {
				AddOutputTextDelegate^ d = gcnew AddOutputTextDelegate(this, &MyForm::AddOutputText);
				Invoke(d, gcnew array<Object^>{Text});
			}
			else {
				Outputfield->AppendText(Text + "\r\n");
			}
		}
		void ClearInputField() {
			if (InputField->InvokeRequired) {
				ClearInputFieldDelegate^ d = gcnew ClearInputFieldDelegate(this, &MyForm::ClearInputField);
				Invoke(d, gcnew array<Object^>{});
			}
			else {
				InputField->Clear();
			}
		}

		void AddClient(System::String^ Text) {
			if (Outputfield->InvokeRequired) {
				AddClientFieldTextDelegate^ d = gcnew AddClientFieldTextDelegate(this, &MyForm::AddClient);
				Invoke(d, gcnew array<Object^>{Text});
			}
			else {
				ClientsField->AppendText(Text + "\r\n");
			}
		}
		void ClearClientField() {
			if (InputField->InvokeRequired) {
				ClearClientFieldDelegate^ d = gcnew ClearClientFieldDelegate(this, &MyForm::ClearClientField);
				Invoke(d, gcnew array<Object^>{});
			}
			else {
				ClientsField->Clear();
			}
		}

		void AddPingFieldText(System::String^ Text) {
			if (Outputfield->InvokeRequired) {
				AddPingFieldTextDelegate^ d = gcnew AddPingFieldTextDelegate(this, &MyForm::AddPingFieldText);
				Invoke(d, gcnew array<Object^>{Text});
			}
			else {
				Pingfield->AppendText(Text + "\r\n");
			}
		}
		void ClearPingField() {
			if (InputField->InvokeRequired) {
				ClearPingFieldDelegate^ d = gcnew ClearPingFieldDelegate(this, &MyForm::ClearPingField);
				Invoke(d, gcnew array<Object^>{});
			}
			else {
				Pingfield->Clear();
			}
		}
		
		void CleanUp() {
			if (ChatClient) delete ChatClient;
			if (ChatServer) delete ChatServer;
		}

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->Outputfield = (gcnew System::Windows::Forms::TextBox());
			this->InputField = (gcnew System::Windows::Forms::TextBox());
			this->ClientsField = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->Pingfield = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// Outputfield
			// 
			this->Outputfield->AccessibleName = L"OutputField";
			this->Outputfield->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->Outputfield->CausesValidation = false;
			this->Outputfield->ForeColor = System::Drawing::Color::DarkOrange;
			this->Outputfield->Location = System::Drawing::Point(12, 37);
			this->Outputfield->Multiline = true;
			this->Outputfield->Name = L"Outputfield";
			this->Outputfield->ReadOnly = true;
			this->Outputfield->Size = System::Drawing::Size(348, 245);
			this->Outputfield->TabIndex = 1;
			// 
			// InputField
			// 
			this->InputField->AccessibleName = L"InputField";
			this->InputField->Location = System::Drawing::Point(12, 288);
			this->InputField->Name = L"InputField";
			this->InputField->Size = System::Drawing::Size(348, 20);
			this->InputField->TabIndex = 0;
			this->InputField->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::InputField_KeyUp);
			// 
			// ClientsField
			// 
			this->ClientsField->AccessibleName = L"ClientsField";
			this->ClientsField->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->ClientsField->CausesValidation = false;
			this->ClientsField->ForeColor = System::Drawing::Color::DarkOrange;
			this->ClientsField->Location = System::Drawing::Point(368, 37);
			this->ClientsField->Multiline = true;
			this->ClientsField->Name = L"ClientsField";
			this->ClientsField->ReadOnly = true;
			this->ClientsField->Size = System::Drawing::Size(139, 245);
			this->ClientsField->TabIndex = 2;
			this->ClientsField->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::DarkOrange;
			this->label1->Location = System::Drawing::Point(365, 13);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(105, 16);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Connected User";
			// 
			// Pingfield
			// 
			this->Pingfield->AccessibleName = L"PingField";
			this->Pingfield->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->Pingfield->CausesValidation = false;
			this->Pingfield->ForeColor = System::Drawing::Color::DarkOrange;
			this->Pingfield->Location = System::Drawing::Point(513, 37);
			this->Pingfield->Multiline = true;
			this->Pingfield->Name = L"Pingfield";
			this->Pingfield->ReadOnly = true;
			this->Pingfield->Size = System::Drawing::Size(61, 245);
			this->Pingfield->TabIndex = 4;
			this->Pingfield->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->ForeColor = System::Drawing::Color::DarkOrange;
			this->label2->Location = System::Drawing::Point(510, 13);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(35, 16);
			this->label2->TabIndex = 5;
			this->label2->Text = L"Ping";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->ForeColor = System::Drawing::Color::DarkOrange;
			this->label3->Location = System::Drawing::Point(12, 13);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(38, 16);
			this->label3->TabIndex = 6;
			this->label3->Text = L"Chat ";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->ClientSize = System::Drawing::Size(586, 320);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->Pingfield);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->ClientsField);
			this->Controls->Add(this->InputField);
			this->Controls->Add(this->Outputfield);
			this->Name = L"MyForm";
			this->Text = L"Chatter";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
		AddOutputText("You want to be the Server or the Client? [S] Server / [C] Client");
	}

	private: System::Void InputField_KeyUp(System::Object^  sender, KeyEventArgs^  e) {
		if (e->KeyCode == Keys::Enter && !ChatMode && Answer == "") {
			Answer = InputField->Text;
			AddOutputText(Answer);

			// Server Mode
			if (Answer == "s" || Answer == "S") {
				AddOutputText("You are now the Server!");
				ChatServer = gcnew Server(this);
				ChatServer->Advertise();
				ChatServer->AcceptClient();
			}

			// Client Mode
			else if (Answer == "c" || Answer == "C") {
				AddOutputText("You are now a Client!");
				AddOutputText("What is your Username?");
			}
		}

		else if (e->KeyCode == Keys::Enter && !ChatMode && UserName == "") {
			UserName = InputField->Text;
			ChatClient = gcnew Client(this, (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(UserName)).ToPointer());
			UDPPackage myPackage;
			myPackage.Header = 'Def';

			while (true) {
				myPackage = ChatClient->Listen();
				if (myPackage.Header != 'Def') {
					break;
				}
			}

			ChatClient->ConnectToServer(myPackage);
			ChatMode = true;
		}
		else if (e->KeyCode == Keys::Enter && ChatMode) {
			if (InputField->Text != "") {
				AddOutputText(UserName + ": "+ InputField->Text);
				ChatClient->SendMessage((const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(InputField->Text)).ToPointer());
			}
		}
		if (e->KeyCode == Keys::Enter) {
			ClearInputField();
		}
	}
	private: void Form1_Closing(Object^ sender, FormClosingEventArgs e)
	{
		if (e.CloseReason == CloseReason::UserClosing)
		{
			CleanUp();
		}
	}
};
}
